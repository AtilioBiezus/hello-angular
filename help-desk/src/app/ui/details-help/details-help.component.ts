import { Component, OnInit } from '@angular/core';
import { HelpDesk } from 'src/app/model/help.model';

@Component({
    selector: 'app-details-help',
    templateUrl: './details-help.component.html',
    styleUrls: ['./details-help.component.css']
})
export class DetailsHelpComponent implements OnInit {

    help: HelpDesk = new HelpDesk()

    constructor() { }

    ngOnInit(): void {

    }

}
