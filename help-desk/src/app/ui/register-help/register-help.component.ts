import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterHelp } from 'src/app/model/help.model';

@Component({
    selector: 'app-register-help',
    templateUrl: './register-help.component.html',
    styleUrls: ['./register-help.component.css']
})
export class RegisterHelpComponent {

    formHelp = this.formBuilder.group({
        name_issuer: ['', [ Validators.required ] ],
        email_issuer: ['', [ Validators.required ] ],
        name_responder: ['', [ Validators.required ] ],
        email_responder: ['', [ Validators.required ] ],
        title: ['', [ Validators.required ] ],
        description: ['', [ Validators.required ] ],
        open_date: ['', [ Validators.required ] ],
        status: ['', [ Validators.required ] ],
        close_date: ['', [ Validators.required ] ]
    })

    constructor(private formBuilder: FormBuilder,
                private router: Router) { }

    save() {
        let help = new RegisterHelp(
            this.name_issuer.value,
            this.email_issuer.value,
            this.title.value,
            this.open_date.value,
            this.email_responder.value,
            this.name_responder.value,
            this.status.value,
            this.description.value,
            this.close_date.value
        )        
    }

    cancel() {
        this.router.navigateByUrl("/")
    }

    get name_issuer() {
        return this.formHelp.controls.name_issuer
    }

    get email_issuer() {
        return this.formHelp.controls.email_issuer
    }

    get name_responder() {
        return this.formHelp.controls.name_responder
    }

    get email_responder() {
        return this.formHelp.controls.email_responder
    }

    get description() {
        return this.formHelp.controls.description
    }

    get title() {
        return this.formHelp.controls.title
    }

    get open_date() {
        return this.formHelp.controls.open_date
    }

    get status() {
        return this.formHelp.controls.status
    }

    get close_date() {
        return this.formHelp.controls.close_date
    }
}
