import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HelpDesk } from "../model/help.model";
import { DefaultResponse } from "../model/response.model";

const URL_BASE = "http://localhost:8080/HelpDesk"
const URL_FIND_BY_TITLE = URL_BASE + "/byTitle?title="
const URL_FIND_BY_ISBN = URL_BASE + "/byIsbn/"


@Injectable({
    providedIn: 'root'
})
export class HelpDeskService {

    constructor(private http: HttpClient) {

    }

    findAll(): Observable< DefaultResponse<HelpDesk[]> > {
        return this.http.get< DefaultResponse<HelpDesk[]> >(URL_BASE)
    }

    findByTitle(title: string): Observable< DefaultResponse<HelpDesk[]>> {
        return this.http.get< DefaultResponse<HelpDesk[]> >(URL_FIND_BY_TITLE + title)
    }
}