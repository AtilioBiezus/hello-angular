export class HelpDesk {
    public ticket_id: number
    public name_issuer: string
    public email_issuer: string
    public name_responder : string
    public email_responder : string
    public title: string
    public description: string
    public open_date : Date
    public status: number
    public close_date : Date
}

export class RegisterHelp {
    public name_issuer: string
    public email_issuer: string
    public name_responder : string
    public email_responder : string
    public title: string
    public description: string
    public open_date : Date
    public status: number
    public close_date : Date

    constructor(name_issuer: string,
                email_issuer: string,
                name_responder : string,
                email_responder : string,
                title: string,
                description: string,
                open_date : Date,
                status: number,
                close_date : Date) {
        
        this.name_issuer = name_issuer
        this.email_issuer = email_issuer
        this.name_issuer = name_responder
        this.email_responder = email_responder
        this.title = title
        this.description = description
        this.open_date = open_date
        this.status = status
        this.close_date = close_date
    }
}