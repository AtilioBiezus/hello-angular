export class DefaultResponse<T> {
    public message: string
    public result: T
}