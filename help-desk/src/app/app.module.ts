import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './ui/home/app.component';
import { ListHelpComponent } from './ui/list-help/list-help.component';
import { RegisterHelpComponent } from './ui/register-help/register-help.component';
import { DetailsHelpComponent } from './ui/details-help/details-help.component';

@NgModule({
  declarations: [
    AppComponent,
    ListHelpComponent,
    RegisterHelpComponent,
    DetailsHelpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
