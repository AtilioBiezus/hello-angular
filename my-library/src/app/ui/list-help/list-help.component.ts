import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HelpDesk } from 'src/app/model/help.model';
import { DefaultResponse } from 'src/app/model/response.model';
import { HelpDeskService } from 'src/app/service/help.service';

@Component({
    selector: 'app-list-help',
    templateUrl: './list-help.component.html',
    styleUrls: ['./list-helps.component.css'],
})
export class ListHelpComponent implements OnInit {

    help: HelpDesk[] = []

    formSearch = this.formBuilder.group({
        searchType: [1, [Validators.required]],
        searchText: ['', [Validators.required]]
    })

    constructor(private formBuilder: FormBuilder, 
                private helpService: HelpDeskService) { }

    ngOnInit() {
        this.findBooks()
    }

    findBooks() {
        this.helpService.findAll().subscribe(
            response => this.onSearchResult(response)
        )
    }

    search() {
        if (this.searchType.value == 1) {
            this.helpService.findByTitle(this.searchText.value).subscribe(
                response => this.onSearchResult(response)
                )
        }
    }

    onSearchResult(response: DefaultResponse<HelpDesk[]>) {
        if (response != null) {
            this.help = response.result
        }
    }

    clearSearch() {
        this.searchText.setValue("")
        this.searchType.setValue(1)
    }

    get searchType() {
        return this.formSearch.controls.searchType
    }

    get searchText() {
        return this.formSearch.controls.searchText
    }
}
