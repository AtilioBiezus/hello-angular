import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsHelpComponent } from './ui/details-help/details-help.component';
import { ListHelpComponent } from './ui/list-help/list-help.component';
import { RegisterHelpComponent } from './ui/register-help/register-help.component';

const routes: Routes = [
    { path: 'list-help', component: ListHelpComponent },
    { path: 'register-help', component: RegisterHelpComponent },
    { path: 'details-help', component: DetailsHelpComponent },
    { path: '', pathMatch: 'full', redirectTo: '/list-help'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
