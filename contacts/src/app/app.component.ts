import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Contact } from './model/contect.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  contactForm = this.formBuilder.group({
    nameContact: new FormControl('', [ Validators.required, Validators.minLength(5) ] ),
    emailContact: new FormControl('', [ Validators.required,  Validators.email] )
  })

  allContacts: Contact[] = []

  constructor(private formBuilder: FormBuilder){
  }

  ngOnInit(){
    let list = localStorage.getItem('contact-list')
    if(list != null){
      this.allContacts = JSON.parse(list)
    }
  }

  save(){
      if(this.contactForm.valid){
      let name = this.contactForm.value.nameContact
      let email = this.contactForm.value.emailContact

      let contact = new Contact(name, email)
      this.allContacts.push(contact)

      localStorage.setItem('contact-list', JSON.stringify(this.allContacts))  

      this. contactForm.reset()
    } 
  }
}