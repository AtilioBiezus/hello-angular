import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";


export class SignupValidators {

    public static nameValidator(control: AbstractControl): ValidationErrors | null {
        let name = control.value.trim()
        let nameParts = name.split(' ')
        

        if(name == '' || nameParts.length >= 2) {
            return null;
        }

        return{
            nameInvalid: true
        };

    }

    public static loginValidator(control: AbstractControl): ValidationErrors | null{
        let login = control.value.toLowerCase()
        let firstLetter = login.charCodeAt(0)

        if(login.indexOf(' ') === -1 && (login.charAt(0) === '_' ) || (firstLetter >= 97 && firstLetter <= 122)){
            return null
        }
        

        return { loginInvalid: true }
    }
}