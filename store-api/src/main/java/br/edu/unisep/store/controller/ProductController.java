package br.edu.unisep.store.controller;

import br.edu.unisep.store.domain.dto.ProductDto;
import br.edu.unisep.store.domain.dto.RegisterProductDto;
import br.edu.unisep.store.domain.usecase.FindAllProductsUseCase;
import br.edu.unisep.store.domain.usecase.FindProductByIdUseCase;
import br.edu.unisep.store.domain.usecase.RegisterProductUseCase;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @GetMapping
    public ResponseEntity<List<ProductDto>> findAll() {
        var useCase = new FindAllProductsUseCase();

        var result = useCase.execute();
        if (result.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> findById(@PathVariable("id") Integer id) {
        var useCase = new FindProductByIdUseCase();

        var product = useCase.execute(id);

        if (product == null) {
            ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(product);

    }

    @PostMapping
    public ResponseEntity<Boolean> save(@RequestBody RegisterProductDto product) {

        var useCase = new RegisterProductUseCase();
        useCase.execute(product);

        return ResponseEntity.ok(true);
    }

}
