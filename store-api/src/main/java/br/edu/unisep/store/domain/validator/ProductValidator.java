package br.edu.unisep.store.domain.validator;

import br.edu.unisep.store.domain.dto.RegisterProductDto;
import org.apache.commons.lang3.Validate;


public class ProductValidator {

    public void validate(RegisterProductDto registerProduct) {

        Validate.notBlank(registerProduct.getName(), "Informe o nome do produto!");
        Validate.isTrue(registerProduct.getName().length() <= 255, "Nome excedeu o número limíte de caracteres!");

        Validate.notBlank(registerProduct.getDescription(),"Informe a descrição do produto!");
        Validate.isTrue(registerProduct.getDescription().length() <= 1024, "Descrição excedeu o número limíte de caracteres!");

        Validate.notNull(registerProduct.getPrice(),"Informe o preço do produto!");
        Validate.isTrue(registerProduct.getPrice() > 0, "O preço do produto não pode ser zero ou menor!");
        Validate.isTrue(registerProduct.getPrice().toString().length() <= 255, "Nome excedeu o número limíte de caracteres!");

        Validate.notBlank(registerProduct.getBrand(),"Informe a marca do produto!");
        Validate.isTrue(registerProduct.getBrand().length() <= 255, "Nome da marca excedeu o número máximo de caracteres!");

        Validate.notNull(registerProduct.getStatus(),"Informe o status do produto!");
        Validate.isTrue(registerProduct.getStatus() >= 0 && registerProduct.getStatus() <= 2,
                "Informe um status válido!");
    }
}
