export class Parking{
    cars: string
    plate: string
    driver: string
    phoneNumber: string
    entrance: string
    finish ?: string
    total ?: number = 0
   
   
    constructor(cars: string, plate: string, driver: string, phoneNumber: string, entrance: string, finish ?: string, total ?: number){
        this.cars = cars
        this.plate = plate
        this.driver = driver
        this.phoneNumber = phoneNumber
        this.entrance = entrance
        this.finish = finish  
        this.total = total   
    }

}