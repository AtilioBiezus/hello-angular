import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Parking } from './model/register-parking.model';

@Component({
  selector: 'app-register-parking',
  templateUrl: './register-parking.component.html',
  styleUrls: ['./register-parking.component.css']
})
export class RegisterParkingComponent implements OnInit {


  parkingForm = this.formBuilder.group({
    cars: ['',  [ Validators.required, Validators.minLength(3) ]  ],
    plate: ['', [ Validators.required, Validators.minLength(7) ] ],
    driver: ['', [ Validators.required ] ],
    phoneNumber: ['', [ Validators.required ] ]
  })
  
  allParking: Parking[] = []

  constructor(private router: Router, private formBuilder: FormBuilder) {   }

  ngOnInit(): void {
    let list = localStorage.getItem('list-parking')
    if  (list != null){
      this.allParking = JSON.parse(list)
    }
  }

  goToList(){
    this.router.navigateByUrl('list-parking')
  }

  save(){
    if (this.parkingForm.valid) {
      let cars = this.parkingForm.value.cars
      let plate = this.parkingForm.value.plate
      let driver = this.parkingForm.value.driver
      let phoneNumber = this.parkingForm.value.phoneNumber
      let entrance = new Date().toISOString()
     
      
      let parking = new Parking(cars, plate, driver, phoneNumber, entrance)
      this.allParking.push(parking)

      localStorage.setItem('list-parking', JSON.stringify(this.allParking))
      this.router.navigateByUrl('list-parking')
      
    }
  }

  

}